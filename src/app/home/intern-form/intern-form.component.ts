import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { InternService } from '../services/intern.service';
import { Router } from '@angular/router';
import { Intern } from '../types/intern.type';
import { PoeService } from '../../poe/services/poe.service.service';

@Component({
  selector: 'app-intern-form',
  templateUrl: './intern-form.component.html',
  styleUrls: ['./intern-form.component.scss']
})
export class InternFormComponent implements OnInit {
  public internForm: FormGroup = new FormGroup({});
  poes: any;

  constructor(
    private _formBuilder: FormBuilder,
    private _internService: InternService,
    private _router: Router,
    private _poeService: PoeService
  ) {}

  ngOnInit(): void {
    this.internForm = this._formBuilder.group({
      lastname: [
        '', // Default value
        [
          Validators.required,
          Validators.minLength(3)
        ]
      ],
      firstname: [
        '',
        [
          Validators.required,
          Validators.minLength(3)
        ]
      ],
      poeId: ['', Validators.required]
    });

    // Récupération des POE
    this._poeService.findAll().subscribe((poes: any) => {
      this.poes = poes;
    });
  }

  onSubmit(): void {
    this._internService.add(this.internForm.value)
      .subscribe((intern: Intern) => {
        this._router.navigate(['/home']);
      });
  }
}
