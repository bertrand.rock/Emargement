import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Poe } from '../../home/types/poe.type';

@Injectable({
  providedIn: 'root'
})
export class PoeService {
  constructor(private httpClient: HttpClient) { }

  add(item: Poe): Observable<Poe> {
    return this.httpClient.post<Poe>('http://localhost:3000/poe', item);
  }

  findAll(): Observable<Poe[]> {
    return this.httpClient.get<Poe[]>('http://localhost:3000/poe');
  }
}
