import { TestBed } from '@angular/core/testing';

import { PoeServiceService } from './poe.service.service';

describe('PoeServiceService', () => {
  let service: PoeServiceService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(PoeServiceService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
