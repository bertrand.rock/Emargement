import { Component, OnInit } from '@angular/core';
import { Poe } from '../../home/types/poe.type';
import { PoeService } from '../services/poe.service.service';
import { map, take } from 'rxjs/operators';

@Component({
  selector: 'app-poe',
  templateUrl: './poe.component.html',
  styleUrls: ['./poe.component.scss']
})
export class PoeComponent implements OnInit {
  poes: Poe[] = [];

  constructor(private poeService: PoeService) {}

  ngOnInit(): void {
    this.poeService.findAll()
      .pipe(
        take(1),
        map((poes: Poe[]) => {
          return poes.sort((p1: Poe, p2: Poe) => {
            return p1.title.localeCompare(p2.title);
          });
        })
      )
      .subscribe((poes: Poe[]) => {
        this.poes = poes;
      });
  }
}
